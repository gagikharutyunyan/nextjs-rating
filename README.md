#### Init project
```bash
npx create-next-app ./ --use-npm
```

#### Touch tsconfig.json
```bash
touch tsconfig.json
```

#### Add typescript
```bash
npm -i D typescript @types/react @types/node
```

#### Eslint
```bash
npm i -D eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

#### StylelintStylelint
```bash
npm i -D stylelint stylelint-config-standard stylelint-order stylelint-order-config-standard --legacy-peer-deps
```

#### ClassName package for @typescript
```bash
npm i -D @types/classnames
```
#### Svgr module | config in next.config.js
```bash
npm i -D @svgr/webpack
```

#### This ESLint plugin enforces the Rules of Hooks.
```bash
npm i eslint-plugin-react-hooks --save-dev
```

### Main Hooks

###### useState -> *позволяет работать с состоянием компонента.*

###### useEffect -> *позволяет подписаться на изменение жизненного цикла компонента, например входящие props-и выполнять какие-то функции в рамках этих изменений.*

###### useContext -> *позволяет использовать глобальный контекст, читать и записывать оттуда значение.*

### Additional Hooks 

###### useReducer -> *похож на state, но позволяет нам больше использовать redux style изминение стейта, вместо его полной замены.*

###### useCallback -> *позволяет нам выполнить какую то функция при изменении входящих параметров.*

###### useMemo -> *запоминает результат этой функции и не будет требовать перерендора при изменении входящих параметров.*

###### useRef -> *отвечает за получение ссылки с нашего компонента на DOM дереве.*

###### useLayoutEffect -> *эффект который триггерится после построения DOM дерева, синхронный эффект, рекомендуется использовать если только если нет другого выхода.*
> (!!! может затормозить все анимации которые есть на странице !!!)(!!! может затормозить все анимации которые есть на странице !!!)

###### useImperativeHandle -> *позволяет прокидовать родительский компонент какие то рефы.*

###### useDebugValue -> *помогает отображать в DevTools-ах какую то величину для дебага.*

###### HOC -> *Это функция, которая принимает компонент и возвращает новый компонент*

Figma
https://www.figma.com/file/eHIyKZXUUtMf1BQiuv6tTA/%D0%9A%D1%83%D1%80%D1%81-2-NextJS?node-id=0%3A1
