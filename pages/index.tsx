import React, { useEffect, useState } from 'react';
import { Button, Htag, Paragraph, Tag } from '../components/';
import {Rating} from "../components/Rating/Rating";
import {withLayout} from "../layout/Layout";

function Home(): JSX.Element {
	const [counter, setCounter] = useState<number>(0);
	const [rating, setRating] = useState<number>(1);


	return (
		<>
			<Htag tag="h2">{counter}</Htag>
			<Button appearance='primary' arrow='right' onClick={() => setCounter(x => x + 1)}>Button</Button>
			<Button appearance='ghost' arrow='down'>Button</Button>
			<Paragraph size='s'>Something</Paragraph>
			<Paragraph size='m'>Something</Paragraph>
			<Paragraph size='l'>Something</Paragraph>
			<Tag size='s' color='green'>Task</Tag>
			<Tag href='https://google.com'>Google</Tag>
			<Rating rating={rating} isEditable={true} setRating={setRating}/>
		</>
	);
}

export default withLayout(Home);
